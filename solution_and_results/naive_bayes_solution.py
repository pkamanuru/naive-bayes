from __future__ import division
import time
import datetime
import math
from ctypes import *



##################
# Training Phase #
##################


# Initializing training file handles
# ----------------------------------
training_input_file = open('../trainingimages.txt', 'r')
training_out_file = open('../traininglabels.txt', 'r')

# Initialize the variables
# ------------------------
# [image#][y_coordinate=row][x_coordinate=col]
black_pixel_cnt = [[[0.0 for x in range(28)] for y in range(28)] for z in range(10)] 
# [y_coordinate=row][x_coordinate=col]
overlap_cnt = [[0.0 for x in range(28)] for y in range(28)]
cur_label = -1
image_line_cnt = 0
training_digit_cnt = [0 for x in range(10)]
total_training_size = 5000
p_prior_digit = [0.0 for x in range(10)]
p_conditional_pixel = [[[0.0 for x in range(28)] for y in range(28)] for z in range(10)]
x_co = 0
y_co = 0
k_laplace_constant = 1.0

# Populate the count Matrices and vectors
# ---------------------------------------
for line in training_input_file:

	# update cur_label when making transition from one image to onother
	if image_line_cnt%28 == 0:
		cur_label = int(training_out_file.readline())
		training_digit_cnt[cur_label] += 1
	
	cur_line = line[0:28]
	cur_line_cnt_in_image = image_line_cnt%28
	y_co = cur_line_cnt_in_image
	# #----------Debug--------- # 
	# print cur_line
	# #------------------------ #

	for itr in range(28):
		if cur_line[itr] != ' ':
			x_co = itr
			black_pixel_cnt[cur_label][y_co][x_co] += 1.0
			overlap_cnt[y_co][x_co] += 1.0

	# Increment image_line_cnt when reading the images line-by-line
	image_line_cnt += 1

# #----------Debug--------- # 
# #for checking pixel_cnt and overlap_cnt matrices
# print image_line_cnt
# print black_pixel_cnt
# print overlap_cnt
# print training_digit_cnt
# #------------------------ #

# Calculate probabilities: calculate in logs
# ------------------------------------------

for digit in range(10):
	p_prior_digit[digit] = math.log(training_digit_cnt[digit]/total_training_size)

for digit in range(10):
	for y in range(28):
		for x in range(28):
			p_conditional_pixel[digit][y][x] = math.log( (black_pixel_cnt[digit][y][x] + k_laplace_constant) \
				/ (overlap_cnt[y][x] + k_laplace_constant*total_training_size) )

# #----------Debug--------- # 
# #for checking probabilities
# print p_prior_digit
# print p_conditional_pixel
# #------------------------ #


#################
# Testing Phase #
#################


# Initializing test file handles
# ----------------------------------
test_input_file = open('../testimages.txt', 'r')
test_out_file = open('../testlabels.txt', 'r')


# Initialize the variables
# ------------------------
predicted_digit = -1
p_predicted_digit = [p_prior_digit[x] for x in range(10)]
digit_wise_success_cnt = [0 for x in range(10)]
overall_success_cnt = 0
total_testing_size = 1000
test_digit_cnt = [0 for x in range(10)]
overall_success_rate = 0.0
digit_wise_success_rate = [0.0 for x in range(10)]
# repeat but for re-initialization/recall
x_co = 0
y_co = 0
cur_label = -1
image_line_cnt = 0


# Predict the labels
# ------------------
for line in test_input_file:

	# update cur_label when making transition from one image to onother
	if image_line_cnt%28 == 0:
		cur_label = int(test_out_file.readline())
		p_predicted_digit = [p_prior_digit[x] for x in range(10)]
		test_digit_cnt[cur_label] += 1	
	
	cur_line = line[0:28]
	cur_line_cnt_in_image = image_line_cnt%28
	y_co = cur_line_cnt_in_image
	# #----------Debug--------- # 
	# print cur_line
	# #------------------------ #

	for itr in range(28):
		x_co = itr
		if cur_line[itr] != ' ':
			for digit in range(10):
				p_predicted_digit[digit] += p_conditional_pixel[digit][y_co][x_co]

	# when this is the last line of the image,
	# lets predict the digit and record success/failure
	if image_line_cnt%28 == 27:
		mp_digit = -1
		p_mp_digit = float("-inf")

		for digit in range(10):
			if p_predicted_digit[digit] > p_mp_digit:
				p_mp_digit = p_predicted_digit[digit]
				mp_digit = digit

		predicted_digit = mp_digit

		if cur_label == predicted_digit:
			overall_success_cnt += 1
			digit_wise_success_cnt[cur_label] += 1

	# Increment image_line_cnt when reading the images line-by-line
	image_line_cnt += 1


# #----------Debug--------- # 
# #for checking probabilities
# print overall_success_cnt
# print digit_wise_success_cnt
# print test_digit_cnt
# #------------------------ #

# Calculating success_rate/accuracy
# ----------------------------------
overall_success_rate = (overall_success_cnt / total_testing_size)*100

for digit in range(10):
	digit_wise_success_rate[digit] = (digit_wise_success_cnt[digit] / test_digit_cnt[digit])*100


print "Overall Accuracy: ",overall_success_rate, "\n"
print "Following is digit_wise accuracy:\n---------------------" 
for digit in range(10):
	print "digit:", digit, "   accuracy:", digit_wise_success_rate[digit]


# #----------Debug--------- # 
# #for checking probabilities
# print training_digit_cnt[5]
# print p_prior_digit[5]
# #------------------------ #


